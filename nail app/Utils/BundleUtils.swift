//
//  BundleUtils.swift
//  nail app
//
//  Created by Bui Tan Phat on 10/15/18.
//  Copyright © 2018 Bui Tan Phat. All rights reserved.
//

import Foundation
import UIKit

class BundleUtils {
    
    static func getBundlePath() -> Bundle? {
        guard
            let bundlePath = Bundle.main.path(forResource: "Templates", ofType: "bundle"),
            let bundle = Bundle(path: bundlePath) else {
                return nil
        }
        return bundle
    }
    
    static func getLightImagePath() -> String? {
        guard
            let bundle = BundleUtils.getBundlePath()?.paths(forResourcesOfType: ".png", inDirectory: "lighting") as [String]? else {
                return nil
            }

        return bundle[0]
    }
    
    static func getShadowImagePath() -> String? {
        guard
            let bundle = BundleUtils.getBundlePath()?.paths(forResourcesOfType: ".png", inDirectory: "lighting") as [String]? else {
                return nil
        }
        
        return bundle[1]
    }
    
    
    static func load_Background() -> [String]? {
        guard
            let bundlePath = Bundle.main.path(forResource: "Templates", ofType: "bundle"),
            let bundle = Bundle(path: bundlePath),
            let pathArray = bundle.paths(forResourcesOfType: ".jpg", inDirectory: "background") as [String]? else {
                return nil
        }
        return pathArray
    }
    
    static func load_Background_Thumb() -> [String]? {
        guard
            let bundlePath = Bundle.main.path(forResource: "Templates", ofType: "bundle"),
            let bundle = Bundle(path: bundlePath),
            var pathArray = bundle.paths(forResourcesOfType: ".png", inDirectory: "background/thumb") as [String]? else {
            return nil
        }

        pathArray.sort(by: { (path1: String, path2: String) -> Bool in
            return path1.count < path2.count
        })
        return pathArray
    }
}
