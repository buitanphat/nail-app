//
//  UIImageUtils.swift
//  nail app
//
//  Created by Bui Tan Phat on 10/11/18.
//  Copyright © 2018 Bui Tan Phat. All rights reserved.
//

import Foundation
import UIKit

class UIImageUtils {
    
    static func setTintColor(currentImage:UIImage, patternImageName: String) -> UIImage? {
        
        let templateImage = currentImage.withRenderingMode(.alwaysTemplate)
        UIGraphicsBeginImageContextWithOptions(currentImage.size, false, 0);
        guard let patternImage = UIImage(named: patternImageName) else {
            UIGraphicsEndImageContext();
            return nil
        }
        UIColor(patternImage: patternImage).set()
        templateImage.draw(in: CGRect(x: 0, y: 0, width: currentImage.size.width, height: currentImage.size.height))
        guard let newImage = UIGraphicsGetImageFromCurrentImageContext() else {
            UIGraphicsEndImageContext();
            return nil
        };
        UIGraphicsEndImageContext();
        return newImage
    }
    
    static func testLightShadow(currentImage:UIImage) -> UIImage? {
        let templateImage = currentImage.withRenderingMode(.alwaysTemplate)
        UIGraphicsBeginImageContextWithOptions(templateImage.size, false, 0);
        let context = UIGraphicsGetCurrentContext()! as CGContext
        //let context = UIGraphicsGetCurrentContext()

//        let nonggiua = UIImage(named: "ngon_giua")
//        let imgView = UIImageView(image: nonggiua!)
//
//        let shadow = UIImageUtils.getShadowTintColor()!
//        let light = UIImageUtils.getLightTintColor()!
//
//        UIColor(patternImage: nonggiua!).setFill()
//        UIColor(patternImage: shadow).setFill()
//        UIColor(patternImage: light).setFill()
//        nonggiua!.draw(in: CGRect(x: 0, y: 0, width: currentImage.size.width, height: currentImage.size.height))
//        shadow.draw(in: CGRect(x: 0, y: 0, width: currentImage.size.width, height: currentImage.size.height))
//        light.draw(in: CGRect(x: 0, y: 0, width: currentImage.size.width, height: currentImage.size.height))
        //UIColor.red.set()
        //UIColor(patternImage: UIImageUtils.getLightTintColor()!).set()


        let nonggiua = UIImage(named: "ngon_giua")
        nonggiua?.draw(in: CGRect(x: 0, y: 0, width: currentImage.size.width, height: currentImage.size.height))

        UIColor(patternImage: UIImageUtils.getLightTintColor()!).set()
        templateImage.draw(in: CGRect(x: 0, y: 0, width: currentImage.size.width, height: currentImage.size.height))
        
        guard let newImage = UIGraphicsGetImageFromCurrentImageContext() else {
            UIGraphicsEndImageContext();
            return nil
        };
        
        UIGraphicsEndImageContext();
        return newImage
        
    }
    
    static func mergeImage(firstImage:UIImage, secondImage:UIImage) -> UIImage? {
        
        print("First Image W = ", firstImage.size.width);
        print("First Image H = " , firstImage.size.height);
        
        print("Second Image W = ", secondImage.size.width);
        print("Second Image H = " , secondImage.size.height);
        
        print("Screen  W = ", UIScreen.main.bounds.size.width);
        print("Screen  H = " , UIScreen.main.bounds.size.height);
        
        UIGraphicsBeginImageContextWithOptions(UIScreen.main.bounds.size, false, 0)
        
        firstImage.draw(in: UIScreen.main.bounds)
        
        let raito = UIScreen.main.bounds.size.width / firstImage.size.width;
        secondImage.draw(in: CGRect(x: 719 * raito, y: 476 * raito, width: 86 * raito , height: 139 * raito), blendMode: .normal, alpha: 1)

        guard let newImage = UIGraphicsGetImageFromCurrentImageContext() else {
            UIGraphicsEndImageContext()
            return nil;
        }
        
        UIGraphicsEndImageContext()
        return newImage
    }
    
    static func drawImageWithRotation(oldImage:UIImage, degrees: CGFloat) -> UIImage? {
        
        let rotatedViewBox: UIView = UIView(frame: CGRect(x: 0, y: 0, width: oldImage.size.width, height: oldImage.size.height))
        let t: CGAffineTransform = CGAffineTransform(rotationAngle: degrees * CGFloat.pi / 180)
        rotatedViewBox.transform = t
        let rotatedSize: CGSize = rotatedViewBox.frame.size
        //Create the bitmap context
        UIGraphicsBeginImageContext(rotatedSize)
        //UIGraphicsBeginImageContextWithOptions(CGSize(width: 188.0, height: 382.0), false, 0)
        guard let bitmap: CGContext = UIGraphicsGetCurrentContext() else {
            UIGraphicsEndImageContext()
            return nil
        }
        //Move the origin to the middle of the image so we will rotate and scale around the center.
        bitmap.translateBy(x: rotatedSize.width / 2, y: rotatedSize.height / 2)
        //Rotate the image context
        bitmap.rotate(by: (degrees * CGFloat.pi / 180))
        //Now, draw the rotated/scaled image into the context
        bitmap.scaleBy(x: 1.0, y: -1.0)
        bitmap.draw(oldImage.cgImage!, in: CGRect(x: -oldImage.size.width / 2, y: -oldImage.size.height / 2, width: oldImage.size.width, height: oldImage.size.height))
        guard let newImage: UIImage = UIGraphicsGetImageFromCurrentImageContext() else {
            UIGraphicsEndImageContext()
            return nil;
        }
        UIGraphicsEndImageContext()
        return newImage
    }
    
    static func getLightTintColor() -> UIImage? {
        guard
            let lightImagePath =  BundleUtils.getLightImagePath(),
            let lightImage = UIImage(contentsOfFile: lightImagePath) else {
            return nil
        }
        return lightImage
    }
    
    static func getShadowTintColor() -> UIImage? {
        guard
            let shadowImagePath =  BundleUtils.getShadowImagePath(),
            let shadowImage = UIImage(contentsOfFile: shadowImagePath) else {
                return nil
        }
        return shadowImage
    }
    
}
