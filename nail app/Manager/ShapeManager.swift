//
//  ShapeManager.swift
//  nail app
//
//  Created by Bui Tan Phat on 10/16/18.
//  Copyright © 2018 Bui Tan Phat. All rights reserved.
//

import Foundation

class ShapeManager {

    static let sharedInstance = ShapeManager()
    
    private var shapeIndex: Int
    private var shapePathsArray: [String]?
    private var shapeThumbPathsArray: [String]?
    
    private init?() {
        guard
            let bundle = BundleUtils.getBundlePath(),
            let shapePathsArray = bundle.paths(forResourcesOfType: ".png", inDirectory: "shape") as [String]?,
            let shapeThumbPathsArray = bundle.paths(forResourcesOfType: ".png", inDirectory: "shape/thumb") as [String]? else {
                return nil
        }
        
        self.shapePathsArray = shapePathsArray
        self.shapeThumbPathsArray = shapeThumbPathsArray
        self.shapeIndex = 2
    }
    
    func setShapreIndex(index: Int) {
         self.shapeIndex = index
    }
    
    func getShapeIndex() -> Int {
        return self.shapeIndex
    }
    
    func getShapePathsArray() -> [String] {
        return self.shapePathsArray!    }
    
    func getShapeThumbPathsArray() -> [String] {
        return self.shapeThumbPathsArray!
    }
}
