//
//  BackgroundManager.swift
//  nail app
//
//  Created by Bui Tan Phat on 10/16/18.
//  Copyright © 2018 Bui Tan Phat. All rights reserved.
//

import Foundation

class BackgroundManager {
    
    static let sharedInstance = BackgroundManager()
    
    private var backgroundIndex: Int
    private var backgroundPathsArray: [String]?
    private var backgroundThumbPathsArray: [String]?
    
    private init?() {
        guard
            let bundle = BundleUtils.getBundlePath(),
            let backgroundPathsArray = bundle.paths(forResourcesOfType: ".jpg", inDirectory: "background") as [String]?,
            let backgroundThumbPathsArray = bundle.paths(forResourcesOfType: ".png", inDirectory: "background/thumb") as [String]? else {
                return nil
        }
        
        self.backgroundPathsArray = backgroundPathsArray
        self.backgroundThumbPathsArray = backgroundThumbPathsArray
        self.backgroundIndex = 0
    }
    
    func setBackgroundIndex(index: Int) {
        self.backgroundIndex = index
    }
    
    func getBackgroundIndex() -> Int {
        return self.backgroundIndex
    }
    
    func getbackgroundPathsArray() -> [String] {
        return self.backgroundPathsArray!
    }
    
    func getbackgroundThumbPathsArray() -> [String] {
        return self.backgroundThumbPathsArray!
    }
}
