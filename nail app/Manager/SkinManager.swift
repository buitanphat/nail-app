//
//  SkinManager.swift
//  nail app
//
//  Created by Bui Tan Phat on 10/16/18.
//  Copyright © 2018 Bui Tan Phat. All rights reserved.
//

import Foundation
class SkinManager {
    
    static let sharedInstance = SkinManager()
    
    private var skinIndex: Int
    private var skinPathsArray: [String]?
    
    private init?() {
        guard
            let bundle = BundleUtils.getBundlePath(),
            let skinPathsArray = bundle.paths(forResourcesOfType: ".png", inDirectory: "handmodel") as [String]? else {
                return nil
        }
        self.skinPathsArray = skinPathsArray
        self.skinIndex = 0
        return
    }
    
    func setSkinIndex(index: Int) {
        self.skinIndex = index
    }
    
    func getSkinIndex() -> Int {
        return self.skinIndex
    }
    
    func getskinPathsArray() -> [String] {
        return self.skinPathsArray!
    }
}
