//
//  HandController.swift
//  nail app
//
//  Created by Bui Tan Phat on 9/18/18.
//  Copyright © 2018 Bui Tan Phat. All rights reserved.
//

import UIKit

class HandController: UIViewController, UIScrollViewDelegate {
    
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var handImgView: UIImageView!
    @IBOutlet weak var ngon_giua: UIImageView!
    @IBOutlet weak var collectionView: UICollectionView!
    
    var collectionDataArray: [String]?
    var currentTabbarItemSelected: Int = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        NSLog("HandController");
        self.scrollView.minimumZoomScale = 1.0;
        self.scrollView.maximumZoomScale = 3.0;
        self.scrollView.bouncesZoom = false;
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(tapAction(sender:)))
        
        self.handImgView.isUserInteractionEnabled = true
        self.handImgView.addGestureRecognizer(tapGestureRecognizer)
        
        
        let nonggiua = UIImage(named: "middle_finger")
        let b = UIImageUtils.testLightShadow(currentImage: nonggiua!)

    }
    
    override func viewDidAppear(_ animated: Bool) {
        //veHinhAnh();
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        return self.handImgView;
    }
    
    @objc func tapAction(sender: UITapGestureRecognizer) {
        
        let touchPoint = sender.location(in: self.handImgView) as CGPoint // Change to whatever view you want the point for
        print("x = ", touchPoint.x)
        print("y = ", touchPoint.y)
        // ngon giữa (180.0 <= x <= 220, 125<= y <= 170)
        // ngon trỏ (260.0 <= x <= 300, 155<= y <= 195)
        // ngon áp út (110.0 <= x <= 150, 155<= y <= 195)
        // ngon út (40.0 <= x <= 70, 235 <= y <= 275)
        // ngon cái (340.0 <= x <= 380, 320 <= y <= 365)
    }
    
    func abc() {
        
    }
}



// MARK: - UICollectionViewDataSource, UICollectionViewDelegate

extension HandController: UICollectionViewDataSource, UICollectionViewDelegate {
    
    public func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        guard let collectionDataArray = self.collectionDataArray else {
            return 0
        }
        return collectionDataArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let reuseIdentifier = "CollectionCell"
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath as IndexPath)
         let  a = cell as? CollectionCell;
        a?.bindViewWithIndex(index: indexPath.row, path: self.collectionDataArray![indexPath.row])
        return a!
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        // handle tap events
        print("You selected cell #\(indexPath.item)!")
    }
}

// MARK: - UITabBarDelegate

extension HandController: UITabBarDelegate {
    public func tabBar(_ tabBar: UITabBar, didSelect item: UITabBarItem) {
        print("\(item.title!)")
        print("\(item.tag)")
        
        if currentTabbarItemSelected == item.tag {
            return
        }
        
        if item.tag == TabBarItemEnum.Background.rawValue {
            self.collectionDataArray?.removeAll()
            self.collectionDataArray = BundleUtils.load_Background_Thumb()
            self.collectionView.reloadData()
        }
    }
}


// MARK: - ABC


func veHinhAnh() {
    
    let background = UIImage(named: "hand_skin_1")
    let ngongiua = UIImage(named: "ngon_giua")
    
    //let image = UIImageUtils.mergeImage(firstImage: background!, secondImage: ngongiua!)
    let abc = UIImageUtils.drawImageWithRotation(oldImage: ngongiua!, degrees: 9.0);
    let eee = UIImageUtils.setTintColor(currentImage: abc!, patternImageName: "diamon_pattern")
    let w = UIImageUtils.mergeImage(firstImage:  background!, secondImage: abc!)
    //self.handImgView.image = image
    
}

