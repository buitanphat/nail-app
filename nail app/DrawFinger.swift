//
//  DrawFinger.swift
//  nail app
//
//  Created by Bui Tan Phat on 10/16/18.
//  Copyright © 2018 Bui Tan Phat. All rights reserved.
//

import Foundation
import UIKit

class DrawFinger {
    
    static func drawFinger(figerType: FingerEnum) {
        switch figerType {
            case .ThumbFinger: DrawFinger.drawThumbFinger()
            case .IndexFinger: DrawFinger.drawIndexFinger()
            case .MiddleFinger: DrawFinger.drawMiddleFinger()
            case .RingFinger: DrawFinger.drawRingFinger()
            case .PinkyFinger: DrawFinger.drawPinkyFinger()
        }
    }
    
    static func drawThumbFinger() {
    }
    
    static func drawIndexFinger() {
    }
    
    static func drawMiddleFinger() {
        //Load Image từ local
        let middleFinger = UIImage(named: "middle_finger")
        
        //Vẽ Shape
    }
    
    static func drawRingFinger() {
        
    }
    
    static func drawPinkyFinger() {
        
    }
}
