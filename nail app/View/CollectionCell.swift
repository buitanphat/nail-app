//
//  SkinCollectionCell.swift
//  nail app
//
//  Created by Bui Tan Phat on 9/20/18.
//  Copyright © 2018 Bui Tan Phat. All rights reserved.
//

import Foundation
import UIKit

class CollectionCell: UICollectionViewCell {
    
    @IBOutlet weak var thumbImgView: UIImageView!

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.layer.cornerRadius = self.bounds.size.width/2;
        self.clipsToBounds = true;
        return ;
    }

    override func prepareForReuse() {
        super.prepareForReuse()
    }
    
    func bindViewWithIndex(index: NSInteger, path: String) {
        self.thumbImgView.image = UIImage(contentsOfFile: path)
        self.thumbImgView.tag = index
    }
}
