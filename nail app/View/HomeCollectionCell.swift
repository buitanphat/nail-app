//
//  HomeCollectionCell.swift
//  nail app
//
//  Created by Bui Tan Phat on 10/17/18.
//  Copyright © 2018 Bui Tan Phat. All rights reserved.
//

import UIKit

class HomeCollectionCell: UICollectionViewCell {
    
    
    @IBOutlet weak var thumbImgView: UIImageView!
    @IBOutlet weak var titleLbl: UILabel!
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func awakeFromNib() {
        self.thumbImgView.layer.cornerRadius = self.thumbImgView.bounds.size.width/2;
        self.thumbImgView.layer.masksToBounds = true;
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
    }
    
    func bindView(_ indexPath: IndexPath) {
        switch indexPath.row {
            case 0:
                self.thumbImgView.image = UIImage(named: "advertisement_ic")
                self.titleLbl.text = "Fun Cam"
            case 1:
                self.thumbImgView.image = UIImage(named: "my_design_ic")
                self.titleLbl.text = "My Designs"
            case 2:
                self.thumbImgView.image = UIImage(named: "girl_nail_ic")
                self.titleLbl.text = "Beauty Circle"
            case 3:
                self.thumbImgView.image = UIImage(named: "tutorial_ic")
                self.titleLbl.text = "Tutorials"
            
        default:
            self.thumbImgView.image = nil
            self.titleLbl.text = ""
        }
    }
}
