//
//  Constant.swift
//  nail app
//
//  Created by Bui Tan Phat on 10/16/18.
//  Copyright © 2018 Bui Tan Phat. All rights reserved.
//

import Foundation

enum TabBarItemEnum: Int {
    case Color = 0
    case NailLooks = 1
    case Shape = 2
    case Skin = 3
    case Background = 4
}

enum FingerEnum: Int {
    case ThumbFinger = 0
    case IndexFinger = 1
    case MiddleFinger = 2
    case RingFinger = 3
    case PinkyFinger = 4
}
